package hr.fer.zemris.math;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class Vector2DTest {
	
	private Vector2D vector2D;
	private static final double DEFAULT_X = -4.5;
	private static final double DEFAULT_Y = 3.2;
	private static final double EPSILON = 1e-6;

	@BeforeEach
	public void initVector2D() {
		vector2D = new Vector2D(DEFAULT_X, DEFAULT_Y);
	}

	@Test
	public void constructorXYComponentsTest() {
		assertTrue(vector2D instanceof Vector2D);
		assertEquals(DEFAULT_X, vector2D.getX());
		assertEquals(DEFAULT_Y, vector2D.getY());
	}
	
	@Test
	public void getXTest() {
		assertEquals(DEFAULT_X, vector2D.getX());
	}
	
	@Test
	public void getYTest() {
		assertEquals(DEFAULT_Y, vector2D.getY());
	}
	
	@Test
	public void getAngle1stQuadrantTest() {
		Vector2D vector2D = new Vector2D(3.4, 8.9);
		assertTrue(Math.abs(1.205883 - vector2D.getAngle()) < EPSILON);
	}
	
	@Test
	public void getAngle2ndQuadrantTest() {
		Vector2D vector2D = new Vector2D(-3.4, 8.9);
		assertTrue(Math.abs(1.935709 - vector2D.getAngle()) < EPSILON);
	}
	
	@Test
	public void getAngle2ndQuadrantExampleTest() {
		assertTrue(Math.abs(2.523448 - vector2D.getAngle()) < EPSILON);
	}
	
	@Test
	public void getAngle3rdQuadrantTest() {
		Vector2D vector2D = new Vector2D(-3.4, -8.9);
		assertTrue(Math.abs(4.347476 - vector2D.getAngle()) < EPSILON);
	}
	
	@Test
	public void getAngle4thQuadrantTest() {
		Vector2D vector2D = new Vector2D(3.4, -8.9);
		assertTrue(Math.abs(5.077302 - vector2D.getAngle()) < EPSILON);
	}
	
	@Test
	public void getAnglePositiveXTest() {
		Vector2D vector2D = new Vector2D(3.4, 0);
		assertTrue(Math.abs(0 - vector2D.getAngle()) < EPSILON);
	}
	
	@Test
	public void getAngleNegativeXTest() {
		Vector2D vector2D = new Vector2D(-3.4, 0);
		assertTrue(Math.abs(Math.PI - vector2D.getAngle()) < EPSILON);
	}
	
	@Test
	public void getAnglePositiveYTest() {
		Vector2D vector2D = new Vector2D(0, 8.9);
		assertTrue(Math.abs(Math.PI/2 - vector2D.getAngle()) < EPSILON);
	}
	
	@Test
	public void getAngleNegativeYTest() {
		Vector2D vector2D = new Vector2D(0, -8.9);
		assertTrue(Math.abs(3*Math.PI/2 - vector2D.getAngle()) < EPSILON);
	}
	
	@Test
	public void getMagnitudeDefaultExampleTest() {
		assertTrue(Math.abs(Math.sqrt(Math.pow(vector2D.getX(),2) + Math.pow(vector2D.getY(),2)) - vector2D.getMagnitude()) < EPSILON);
	}
	
	@Test
	public void getMagnitudeOnlyXComponentTest() {
		Vector2D vector2D = new Vector2D(3.4, 0);
		assertTrue(Math.abs(vector2D.getX() - vector2D.getMagnitude()) < EPSILON);
	}
	
	@Test
	public void getMagnitudeOnlyYComponentTest() {
		Vector2D vector2D = new Vector2D(0, 3.4);
		assertTrue(Math.abs(vector2D.getY() - vector2D.getMagnitude()) < EPSILON);
	}
	
	@Test
	public void translateNullTest() {
		assertThrows(NullPointerException.class, () -> vector2D.translate(null));
	}
	
	@Test
	public void translateNegativeXPositiveYTest() {
		Vector2D offset = new Vector2D(-5.76, 3.09);
		vector2D.translate(offset);
		assertTrue(Math.abs(-10.26-vector2D.getX()) < EPSILON);
		assertTrue(Math.abs(6.29-vector2D.getY()) < EPSILON);
	}
	
	@Test
	public void translatePositiveXNegativeYTest() {
		Vector2D offset = new Vector2D(5.76, -3.8);
		vector2D.translate(offset);
		assertTrue(Math.abs(1.26-vector2D.getX()) < EPSILON);
		assertTrue(Math.abs(-0.6-vector2D.getY()) < EPSILON);
	}
	
	@Test
	public void translateZeroXTest() {
		Vector2D offset = new Vector2D(0, -3.8);
		vector2D.translate(offset);
		assertTrue(Math.abs(-4.5-vector2D.getX()) < EPSILON);
		assertTrue(Math.abs(-0.6-vector2D.getY()) < EPSILON);
	}
	
	@Test
	public void translateZeroYTest() {
		Vector2D offset = new Vector2D(-3.8, 0);
		vector2D.translate(offset);
		assertTrue(Math.abs(-8.3-vector2D.getX()) < EPSILON);
		assertTrue(Math.abs(3.2-vector2D.getY()) < EPSILON);
	}
	
	@Test
	public void translateZeroXYTest() {
		Vector2D offset = new Vector2D(0, 0);
		vector2D.translate(offset);
		assertTrue(Math.abs(-4.5-vector2D.getX()) < EPSILON);
		assertTrue(Math.abs(3.2-vector2D.getY()) < EPSILON);
	}
	
	
	@Test
	public void translatedNullTest() {
		assertThrows(NullPointerException.class, () -> vector2D.translated(null));
		
		checkIfOriginalVectorUnchanged();
	}
	
	@Test
	public void translatedNegativeXPositiveYTest() {
		Vector2D offset = new Vector2D(-5.76, 3.09);
		Vector2D newVector2D = vector2D.translated(offset);
		assertTrue(Math.abs(-10.26-newVector2D.getX()) < EPSILON);
		assertTrue(Math.abs(6.29-newVector2D.getY()) < EPSILON);
		
		checkIfOriginalVectorUnchanged();
	}
	
	@Test
	public void translatedPositiveXNegativeYTest() {
		Vector2D offset = new Vector2D(5.76, -3.8);
		Vector2D newVector2D = vector2D.translated(offset);
		assertTrue(Math.abs(1.26-newVector2D.getX()) < EPSILON);
		assertTrue(Math.abs(-0.6-newVector2D.getY()) < EPSILON);
		
		checkIfOriginalVectorUnchanged();
	}
	
	@Test
	public void translatedZeroXTest() {
		Vector2D offset = new Vector2D(0, -3.8);
		Vector2D newVector2D = vector2D.translated(offset);
		assertTrue(Math.abs(-4.5-newVector2D.getX()) < EPSILON);
		assertTrue(Math.abs(-0.6-newVector2D.getY()) < EPSILON);
		
		checkIfOriginalVectorUnchanged();
	}
	
	@Test
	public void translatedZeroYTest() {
		Vector2D offset = new Vector2D(-3.8, 0);
		Vector2D newVector2D = vector2D.translated(offset);
		assertTrue(Math.abs(-8.3-newVector2D.getX()) < EPSILON);
		assertTrue(Math.abs(3.2-newVector2D.getY()) < EPSILON);
		
		checkIfOriginalVectorUnchanged();
	}
	
	@Test
	public void translatedZeroXYTest() {
		Vector2D offset = new Vector2D(0, 0);
		Vector2D newVector2D = vector2D.translated(offset);
		assertTrue(Math.abs(-4.5-newVector2D.getX()) < EPSILON);
		assertTrue(Math.abs(3.2-newVector2D.getY()) < EPSILON);
		
		checkIfOriginalVectorUnchanged();
	}


	
	@Test
	public void rotateForPositiveAcuteAngleTest() {
		vector2D.rotate(0.78);
	
		assertTrue(Math.abs(-5.449605-vector2D.getX()) < EPSILON);
		assertTrue(Math.abs(-0.889834-vector2D.getY()) < EPSILON);
	}
	
	@Test
	public void rotateForNegativeAcuteAngleTest() {
		vector2D.rotate(-1.423);
	
		assertTrue(Math.abs(2.502448-vector2D.getX()) < EPSILON);
		assertTrue(Math.abs(4.922169-vector2D.getY()) < EPSILON);
	}
	
	@Test
	public void rotateForPositiveObtuseAngleTest() {
		vector2D.rotate(2.45);
		assertTrue(Math.abs(1.425193-vector2D.getX()) < EPSILON);
		assertTrue(Math.abs(-5.334681-vector2D.getY()) < EPSILON);
	}
	
	@Test
	public void rotateForNegativeObtuseAngleTest() {
		vector2D.rotate(-1.987);
	
		assertTrue(Math.abs(4.746127-vector2D.getX()) < EPSILON);
		assertTrue(Math.abs(2.822105-vector2D.getY()) < EPSILON);
	}
	
	@Test
	public void rotateForPositiveReflexAngleTest() {
		vector2D.rotate(4.55);
		assertTrue(Math.abs(3.885443-vector2D.getX()) < EPSILON);
		assertTrue(Math.abs(3.923434-vector2D.getY()) < EPSILON);
	}
	
	@Test
	public void rotateForNegativeReflexAngleTest() {
		vector2D.rotate(-6.22);
		assertTrue(Math.abs(-4.693079-vector2D.getX()) < EPSILON);
		assertTrue(Math.abs(2.90947-vector2D.getY()) < EPSILON);
	}
	
	@Test
	public void rotateForZeroTest() {
		vector2D.rotate(0);
	
		assertTrue(Math.abs(-4.5-vector2D.getX()) < EPSILON);
		assertTrue(Math.abs(3.2-vector2D.getY()) < EPSILON);
	}
	
	@Test
	public void rotateForPiHalfTest() {
		vector2D.rotate(Math.PI/2);
	
		assertTrue(Math.abs(-3.2-vector2D.getX()) < EPSILON);
		assertTrue(Math.abs(-4.5-vector2D.getY()) < EPSILON);
	}
	
	@Test
	public void rotateForNegativePiHalfTest() {
		vector2D.rotate(-Math.PI/2);
	
		assertTrue(Math.abs(3.2-vector2D.getX()) < EPSILON);
		assertTrue(Math.abs(4.5-vector2D.getY()) < EPSILON);
	}
	
	@Test
	public void rotateForPiTest() {
		vector2D.rotate(Math.PI);
	
		assertTrue(Math.abs(4.5-vector2D.getX()) < EPSILON);
		assertTrue(Math.abs(-3.2-vector2D.getY()) < EPSILON);
	}
	
	@Test
	public void rotateForNegativePiTest() {
		vector2D.rotate(-Math.PI);
	
		assertTrue(Math.abs(4.5-vector2D.getX()) < EPSILON);
		assertTrue(Math.abs(-3.2-vector2D.getY()) < EPSILON);
	}
	
	@Test
	public void rotateFor3PiHalvesTest() {
		vector2D.rotate(3*Math.PI/2);
	
		assertTrue(Math.abs(3.2-vector2D.getX()) < EPSILON);
		assertTrue(Math.abs(4.5-vector2D.getY()) < EPSILON);
	}
	
	@Test
	public void rotateForNegative3PiHalvesTest() {
		vector2D.rotate(-3*Math.PI/2);
	
		assertTrue(Math.abs(-3.2-vector2D.getX()) < EPSILON);
		assertTrue(Math.abs(-4.5-vector2D.getY()) < EPSILON);
	}
	
	@Test
	public void rotateFor2PiTest() {
		vector2D.rotate(2*Math.PI);
	
		assertTrue(Math.abs(-4.5-vector2D.getX()) < EPSILON);
		assertTrue(Math.abs(3.2-vector2D.getY()) < EPSILON);
	}
	
	@Test
	public void rotateForNegative2PiTest() {
		vector2D.rotate(-2*Math.PI);
	
		assertTrue(Math.abs(-4.5-vector2D.getX()) < EPSILON);
		assertTrue(Math.abs(3.2-vector2D.getY()) < EPSILON);
	}
	
	@Test
	public void rotateFor3PiTest() {
		vector2D.rotate(3*Math.PI);
	
		assertTrue(Math.abs(4.5-vector2D.getX()) < EPSILON);
		assertTrue(Math.abs(-3.2-vector2D.getY()) < EPSILON);
	}
	
	@Test
	public void rotateForNegative3PiTest() {
		vector2D.rotate(-3*Math.PI);
	
		assertTrue(Math.abs(4.5-vector2D.getX()) < EPSILON);
		assertTrue(Math.abs(-3.2-vector2D.getY()) < EPSILON);
	}
	
	@Test
	public void rotateFor4PiTest() {
		vector2D.rotate(4*Math.PI);
	
		assertTrue(Math.abs(-4.5-vector2D.getX()) < EPSILON);
		assertTrue(Math.abs(3.2-vector2D.getY()) < EPSILON);
	}
	
	@Test
	public void rotateForNegative4PiTest() {
		vector2D.rotate(-4*Math.PI);
	
		assertTrue(Math.abs(-4.5-vector2D.getX()) < EPSILON);
		assertTrue(Math.abs(3.2-vector2D.getY()) < EPSILON);
	}

	
	@Test
	public void rotatedForPositiveAcuteAngleTest() {
		Vector2D newVector2D = vector2D.rotated(0.78);
	
		assertTrue(Math.abs(-5.449605-newVector2D.getX()) < EPSILON);
		assertTrue(Math.abs(-0.889834-newVector2D.getY()) < EPSILON);
		
		checkIfOriginalVectorUnchanged();
	}
	
	@Test
	public void rotatedForNegativeAcuteAngleTest() {
		Vector2D newVector2D = vector2D.rotated(-1.423);
	
		assertTrue(Math.abs(2.502448-newVector2D.getX()) < EPSILON);
		assertTrue(Math.abs(4.922169-newVector2D.getY()) < EPSILON);
		
		checkIfOriginalVectorUnchanged();
	}
	
	@Test
	public void rotatedForPositiveObtuseAngleTest() {
		Vector2D newVector2D = vector2D.rotated(2.45);
		
		assertTrue(Math.abs(1.425193-newVector2D.getX()) < EPSILON);
		assertTrue(Math.abs(-5.334681-newVector2D.getY()) < EPSILON);
		
		checkIfOriginalVectorUnchanged();
	}
	
	@Test
	public void rotatedForNegativeObtuseAngleTest() {
		Vector2D newVector2D = vector2D.rotated(-1.987);
	
		assertTrue(Math.abs(4.746127-newVector2D.getX()) < EPSILON);
		assertTrue(Math.abs(2.822105-newVector2D.getY()) < EPSILON);
		
		checkIfOriginalVectorUnchanged();
	}
	
	@Test
	public void rotatedForPositiveReflexAngleTest() {
		Vector2D newVector2D = vector2D.rotated(4.55);
		
		assertTrue(Math.abs(3.885443-newVector2D.getX()) < EPSILON);
		assertTrue(Math.abs(3.923434-newVector2D.getY()) < EPSILON);
		
		checkIfOriginalVectorUnchanged();
	}
	
	@Test
	public void rotatedForNegativeReflexAngleTest() {
		Vector2D newVector2D = vector2D.rotated(-6.22);
		
		assertTrue(Math.abs(-4.693079-newVector2D.getX()) < EPSILON);
		assertTrue(Math.abs(2.90947-newVector2D.getY()) < EPSILON);
		
		checkIfOriginalVectorUnchanged();
	}
	
	@Test
	public void rotatedForZeroTest() {
		Vector2D newVector2D = vector2D.rotated(0);
	
		assertTrue(Math.abs(-4.5-newVector2D.getX()) < EPSILON);
		assertTrue(Math.abs(3.2-newVector2D.getY()) < EPSILON);
		
		checkIfOriginalVectorUnchanged();
	}
	
	@Test
	public void rotatedForPiHalfTest() {
		Vector2D newVector2D = vector2D.rotated(Math.PI/2);

	
		assertTrue(Math.abs(-3.2-newVector2D.getX()) < EPSILON);
		assertTrue(Math.abs(-4.5-newVector2D.getY()) < EPSILON);
		
		checkIfOriginalVectorUnchanged();
	}
	
	@Test
	public void rotatedForNegativePiHalfTest() {
		Vector2D newVector2D = vector2D.rotated(-Math.PI/2);
	
		assertTrue(Math.abs(3.2-newVector2D.getX()) < EPSILON);
		assertTrue(Math.abs(4.5-newVector2D.getY()) < EPSILON);
		
		checkIfOriginalVectorUnchanged();
	}
	
	@Test
	public void rotatedForPiTest() {
		Vector2D newVector2D = vector2D.rotated(Math.PI);
	
		assertTrue(Math.abs(4.5-newVector2D.getX()) < EPSILON);
		assertTrue(Math.abs(-3.2-newVector2D.getY()) < EPSILON);
		
		checkIfOriginalVectorUnchanged();
	}
	
	@Test
	public void rotatedForNegativePiTest() {
		Vector2D newVector2D = vector2D.rotated(-Math.PI);
	
		assertTrue(Math.abs(4.5-newVector2D.getX()) < EPSILON);
		assertTrue(Math.abs(-3.2-newVector2D.getY()) < EPSILON);
		
		checkIfOriginalVectorUnchanged();
	}
	
	@Test
	public void rotatedFor3PiHalvesTest() {
		Vector2D newVector2D = vector2D.rotated(3*Math.PI/2);
	
		assertTrue(Math.abs(3.2-newVector2D.getX()) < EPSILON);
		assertTrue(Math.abs(4.5-newVector2D.getY()) < EPSILON);
		
		checkIfOriginalVectorUnchanged();
	}
	
	@Test
	public void rotatedForNegative3PiHalvesTest() {
		Vector2D newVector2D = vector2D.rotated(-3*Math.PI/2);
	
		assertTrue(Math.abs(-3.2-newVector2D.getX()) < EPSILON);
		assertTrue(Math.abs(-4.5-newVector2D.getY()) < EPSILON);
		
		checkIfOriginalVectorUnchanged();
	}
	
	
	@Test
	public void rotatedFor2PiTest() {
		Vector2D newVector2D = vector2D.rotated(2*Math.PI);
	
		assertTrue(Math.abs(-4.5-newVector2D.getX()) < EPSILON);
		assertTrue(Math.abs(3.2-newVector2D.getY()) < EPSILON);
		
		checkIfOriginalVectorUnchanged();
	}
	
	@Test
	public void rotatedForNegative2PiTest() {
		Vector2D newVector2D = vector2D.rotated(-2*Math.PI);
	
		assertTrue(Math.abs(-4.5-newVector2D.getX()) < EPSILON);
		assertTrue(Math.abs(3.2-newVector2D.getY()) < EPSILON);
		
		checkIfOriginalVectorUnchanged();
	}
	
	@Test
	public void rotatedFor3PiTest() {
		Vector2D newVector2D = vector2D.rotated(3*Math.PI);
	
		assertTrue(Math.abs(4.5-newVector2D.getX()) < EPSILON);
		assertTrue(Math.abs(-3.2-newVector2D.getY()) < EPSILON);
		
		checkIfOriginalVectorUnchanged();
	}
	
	@Test
	public void rotatedForNegative3PiTest() {
		Vector2D newVector2D = vector2D.rotated(-3*Math.PI);
	
		assertTrue(Math.abs(4.5-newVector2D.getX()) < EPSILON);
		assertTrue(Math.abs(-3.2-newVector2D.getY()) < EPSILON);
		
		checkIfOriginalVectorUnchanged();
	}
	
	@Test
	public void rotatedFor4PiTest() {
		Vector2D newVector2D = vector2D.rotated(4*Math.PI);
	
		assertTrue(Math.abs(-4.5-newVector2D.getX()) < EPSILON);
		assertTrue(Math.abs(3.2-newVector2D.getY()) < EPSILON);
		
		checkIfOriginalVectorUnchanged();
	}
	
	@Test
	public void rotatedForNegative4PiTest() {
		
		Vector2D newVector2D = vector2D.rotated(-4*Math.PI);
	
		assertTrue(Math.abs(-4.5-newVector2D.getX()) < EPSILON);
		assertTrue(Math.abs(3.2-newVector2D.getY()) < EPSILON);
		
		checkIfOriginalVectorUnchanged();
	}
	
	@Test
	public void scaleByOneTest() {
		vector2D.scale(1.0);
		
		assertTrue(Math.abs(-4.5-vector2D.getX()) < EPSILON);
		assertTrue(Math.abs(3.2-vector2D.getY()) < EPSILON);
	}
	
	@Test
	public void scaleByNegativeOneTest() {
		vector2D.scale(-1.0);
		
		assertTrue(Math.abs(4.5-vector2D.getX()) < EPSILON);
		assertTrue(Math.abs(-3.2-vector2D.getY()) < EPSILON);
	}
	
	@Test
	public void scaleByPositiveNumberTest() {
		vector2D.scale(2.34);
		
		assertTrue(Math.abs(-10.53-vector2D.getX()) < EPSILON);
		assertTrue(Math.abs(7.488-vector2D.getY()) < EPSILON);
	}
	
	@Test
	public void scaleByNegativeNumberTest() {
		vector2D.scale(-2.34);

		assertTrue(Math.abs(10.53-vector2D.getX()) < EPSILON);
		assertTrue(Math.abs(-7.488-vector2D.getY()) < EPSILON);
	}
	
	@Test
	public void scaleByPositiveFractionTest() {
		vector2D.scale(0.34);
		
		assertTrue(Math.abs(-1.53-vector2D.getX()) < EPSILON);
		assertTrue(Math.abs(1.088-vector2D.getY()) < EPSILON);
	}
	
	@Test
	public void scaleByNegativeFractionTest() {
		vector2D.scale(-0.34);
		
		assertTrue(Math.abs(1.53-vector2D.getX()) < EPSILON);
		assertTrue(Math.abs(-1.088-vector2D.getY()) < EPSILON);
	}
	
	@Test
	public void scaledByOneTest() {
		Vector2D newVector2D = vector2D.scaled(1.0);
		
		assertTrue(Math.abs(-4.5-newVector2D.getX()) < EPSILON);
		assertTrue(Math.abs(3.2-newVector2D.getY()) < EPSILON);
	}
	
	@Test
	public void scaledByNegativeOneTest() {
		Vector2D newVector2D = vector2D.scaled(-1.0);
		
		assertTrue(Math.abs(4.5-newVector2D.getX()) < EPSILON);
		assertTrue(Math.abs(-3.2-newVector2D.getY()) < EPSILON);
	}
	
	@Test
	public void scaledByPositiveNumberTest() {
		Vector2D newVector2D = vector2D.scaled(2.34);
		
		assertTrue(Math.abs(-10.53-newVector2D.getX()) < EPSILON);
		assertTrue(Math.abs(7.488-newVector2D.getY()) < EPSILON);
	}
	
	@Test
	public void scaledByNegativeNumberTest() {
		Vector2D newVector2D = vector2D.scaled(-2.34);
		
		assertTrue(Math.abs(10.53-newVector2D.getX()) < EPSILON);
		assertTrue(Math.abs(-7.488-newVector2D.getY()) < EPSILON);
	}
	
	@Test
	public void scaledByPositiveFractionTest() {
		Vector2D newVector2D = vector2D.scaled(0.34);
		
		assertTrue(Math.abs(-1.53-newVector2D.getX()) < EPSILON);
		assertTrue(Math.abs(1.088-newVector2D.getY()) < EPSILON);
	}
	
	@Test
	public void scaledByNegativeFractionTest() {
		Vector2D newVector2D = vector2D.scaled(-0.34);
		
		assertTrue(Math.abs(1.53-newVector2D.getX()) < EPSILON);
		assertTrue(Math.abs(-1.088-newVector2D.getY()) < EPSILON);
	}
	
	@Test
	public void copyTest() {
		Vector2D newVector2D = vector2D.copy();
		
		assertEquals(vector2D, newVector2D);
	}
	
	
	
	
	
	private void checkIfOriginalVectorUnchanged() {
		assertEquals(-4.5, vector2D.getX());
		assertEquals(3.2, vector2D.getY());
	}
	

}
