package hr.fer.zemris.java.custom.collections;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class DictionaryTest {
	
	private Dictionary<String, Integer> dict;
	
	@BeforeEach
	public void initDict() {
		dict = new Dictionary<>();
	}
	

	@Test
	public void defaultConstructorTest() {
		assertTrue(dict instanceof Dictionary);
	}
	
	@Test
	public void emptyDictionaryIsEmptyTest() {
		assertTrue(dict.isEmpty());
	}
	
	
	@Test
	public void emptyDictionarySizeTest() {
		assertEquals(0, dict.size());
	}
	
	@Test
	public void emptyDictionaryClearTest() {
		assertTrue(dict.isEmpty());
		assertEquals(0, dict.size());
		
		dict.clear();
		
		assertTrue(dict.isEmpty());
		assertEquals(0, dict.size());
	}
	
	
	@Test
	public void filledDictionaryIsEmptyTest() {
		prepareDictionary();
		assertFalse(dict.isEmpty());
	}
	
	
	@Test
	public void filledDictionarySizeTest() {
		prepareDictionary();
		assertEquals(4, dict.size());
	}
	
	@Test
	public void filledDictionaryClearTest() {
		prepareDictionary();
		assertFalse(dict.isEmpty());
		assertEquals(4, dict.size());
		
		dict.clear();
		
		assertTrue(dict.isEmpty());
		assertEquals(0, dict.size());
	}
	
	
	
	
	@Test
	public void putNonNullKeysValuesTest() {
		prepareDictionary();
		
		assertFalse(dict.isEmpty());
		assertEquals(4, dict.size());
		assertEquals(10, dict.get("table"));
		assertEquals(4, dict.get("chair"));
		assertEquals(-3, dict.get("backpack"));
		assertEquals(0, dict.get("spoon"));
	}
	
	@Test
	public void putNonNullKeysDuplicatesValuesTest() {
		prepareDictionary();
		dict.put("table", 2);
		dict.put("chair", 30);
		
		assertFalse(dict.isEmpty());
		assertEquals(4, dict.size());
		assertEquals(2, dict.get("table"));
		assertEquals(30, dict.get("chair"));
		assertEquals(-3, dict.get("backpack"));
		assertEquals(0, dict.get("spoon"));
	}
	
	@Test
	public void putNonNullKeysNullValuesTest() {
		prepareDictionary();
		dict.put("armchair", null);
		dict.put("window", null);
		dict.put("ball", null);
		dict.put("table", null);
		
		assertFalse(dict.isEmpty());
		assertEquals(7, dict.size());
		assertNull(dict.get("table"));
		assertEquals(4, dict.get("chair"));
		assertEquals(-3, dict.get("backpack"));
		assertEquals(0, dict.get("spoon"));
		assertNull(dict.get("armchair"));
		assertNull(dict.get("window"));
		assertNull(dict.get("ball"));
	}
	
	@Test
	public void putNullKeysNullAndNonNullValuesTest() {
		prepareDictionary();
		assertThrows(NullPointerException.class, () -> dict.put(null, 4));
		assertThrows(NullPointerException.class, () -> dict.put(null, null));
	}

	@Test
	public void getValueFromExistingKeyTest() {
		prepareDictionary();
		
		assertEquals(10, dict.get("table"));
		assertEquals(4, dict.get("chair"));
		assertEquals(-3, dict.get("backpack"));
		assertEquals(0, dict.get("spoon"));
	}
	
	@Test
	public void getValueFromNullKeyTest() {
		prepareDictionary();
		
		assertThrows(NullPointerException.class, () -> dict.get(null));
	}
	
	@Test
	public void getNullValueFromExistingKeyTest() {
		prepareDictionary();
		dict.put("cloud", null);
		dict.put("rabbit", null);
		
		assertNull(dict.get("cloud"));
		assertNull(dict.get("rabbit"));
		assertNotNull(dict.get("table"));
	}
	
	
	@Test
	public void getValueFromNonexistingKeysTest() {
		prepareDictionary();
		
		assertNull(dict.get("window"));
		assertNull(dict.get("cloud"));
		assertNull(dict.get("rabbit"));
	}

	
	
	
	
	private void prepareDictionary() {
		dict.put("table", 10);
		dict.put("chair", 4);
		dict.put("backpack", -3);
		dict.put("spoon", 0);
	}

}
