package hr.fer.zemris.java.custom.collections;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class ArrayIndexedCollectionTest {

	@Test
	public void defaultConstructorTest() {
		ArrayIndexedCollection<String> arrayIndexedCollection = new ArrayIndexedCollection<>();
		assertEquals(16, ArrayIndexedCollection.getCapacity());
		assertEquals(0, arrayIndexedCollection.size());
	}

	@Test
	public void initialCapacityTenConstructorTest() {
		ArrayIndexedCollection<String> arrayIndexedCollection = new ArrayIndexedCollection<>(10);
		assertEquals(10, ArrayIndexedCollection.getCapacity());
		assertEquals(0, arrayIndexedCollection.size());
	}

	@Test
	public void initialCapacityOneConstructorTest() {
		ArrayIndexedCollection<String> arrayIndexedCollection = new ArrayIndexedCollection<>(1);
		assertEquals(1, ArrayIndexedCollection.getCapacity());
		assertEquals(0, arrayIndexedCollection.size());
	}

	@Test
	public void initialCapacityZeroConstructorTest() {
		assertThrows(IllegalArgumentException.class, () -> new ArrayIndexedCollection<String>(0));
	}

	@Test
	public void initialCapacityNegativeOneConstructorTest() {
		assertThrows(IllegalArgumentException.class, () -> new ArrayIndexedCollection<String>(-1));
	}

	@Test
	public void otherCollectionOfSizeThreeConstructorTest() {
		ArrayIndexedCollection<String> arrayIndexedCollection = new ArrayIndexedCollection<>(prepareOtherCollection());
		assertEquals(3, ArrayIndexedCollection.getCapacity());
		assertEquals(3, arrayIndexedCollection.size());
	}

	@Test
	public void otherCollectionNullConstructorTest() {
		assertThrows(NullPointerException.class, () -> new ArrayIndexedCollection<String>(null));
	}

	@Test
	public void otherCollectionOfSizeThreeInitialCapacityTenConstructorTest() {
		ArrayIndexedCollection<String> arrayIndexedCollection = new ArrayIndexedCollection<>(prepareOtherCollection(), 10);
		assertEquals(10, ArrayIndexedCollection.getCapacity());
		assertEquals(3, arrayIndexedCollection.size());
	}

	@Test
	public void otherCollectionOfSizeTwoInitialCapacityOneConstructorTest() {
		ArrayIndexedCollection<String> arrayIndexedCollection = new ArrayIndexedCollection<>(prepareOtherCollection(), 1);
		assertEquals(3, ArrayIndexedCollection.getCapacity());
		assertEquals(3, arrayIndexedCollection.size());
	}

	@Test
	public void otherCollectionOfSizeTwoInitialCapacityZeroConstructorTest() {
		assertThrows(IllegalArgumentException.class, () -> new ArrayIndexedCollection<String>(prepareOtherCollection(), 0));
	}

	@Test
	public void otherCollectionNullInitialCapacityTenConstructorTest() {
		assertThrows(NullPointerException.class, () -> new ArrayIndexedCollection<String>(null, 10));
	}

	@Test
	public void otherCollectionNullInitialCapacityZeroConstructorTest() {
		assertThrows(IllegalArgumentException.class, () -> new ArrayIndexedCollection<String>(null, 0));
	}

	@Test
	public void collectionOfSizeThreeSize() {
		assertEquals(3, prepareOtherCollection().size());
	}

	@Test
	public void collectionOfSizeZeroSize() {
		assertEquals(0, new ArrayIndexedCollection<String>().size());
	}

	@Test
	public void collectionThreeNonNullElementsWithDuplicatesAdd() {
		ArrayIndexedCollection<String> arrayIndexedCollection = new ArrayIndexedCollection<>();
		arrayIndexedCollection.add(new String("A"));
		arrayIndexedCollection.add(new String("B"));
		arrayIndexedCollection.add(new String("A"));

		assertEquals(3, arrayIndexedCollection.size());
		assertEquals("A", arrayIndexedCollection.get(0));
		assertEquals("B", arrayIndexedCollection.get(1));
		assertEquals("A", arrayIndexedCollection.get(2));
	}

	@Test
	public void collectionNullElementAdd() {
		ArrayIndexedCollection<String> arrayIndexedCollection = new ArrayIndexedCollection<>();
		arrayIndexedCollection.add(new String("A"));
		arrayIndexedCollection.add(new String("B"));
		assertThrows(NullPointerException.class, () -> arrayIndexedCollection.add(null));
	}

	@Test
	public void collectionThreeElementsContainsNonNull() {
		Collection<String> arrayIndexedCollection = prepareOtherCollection();
		assertTrue(arrayIndexedCollection.contains("A"));
		assertTrue(arrayIndexedCollection.contains("B"));
		assertTrue(arrayIndexedCollection.contains("C"));
	}

	@Test
	public void collectionThreeElementsContainsNull() {
		Collection<String> arrayIndexedCollection = prepareOtherCollection();
		assertFalse(arrayIndexedCollection.contains(null));
	}

	@Test
	public void collectionThreeElementsRemoveExisting() {
		Collection<String> arrayIndexedCollection = prepareOtherCollection();

		assertTrue(arrayIndexedCollection.contains("A"));
		assertTrue(arrayIndexedCollection.contains("B"));
		assertTrue(arrayIndexedCollection.contains("C"));
		assertEquals(3, arrayIndexedCollection.size());

		assertTrue(arrayIndexedCollection.remove("C"));

		assertTrue(arrayIndexedCollection.contains("A"));
		assertTrue(arrayIndexedCollection.contains("B"));
		assertFalse(arrayIndexedCollection.contains("C"));
		assertEquals(2, arrayIndexedCollection.size());
	}

	@Test
	public void collectionThreeElementsRemoveNonexisting() {
		Collection<String> arrayIndexedCollection = prepareOtherCollection();

		assertTrue(arrayIndexedCollection.contains("A"));
		assertTrue(arrayIndexedCollection.contains("B"));
		assertTrue(arrayIndexedCollection.contains("C"));
		assertFalse(arrayIndexedCollection.contains("D"));
		assertEquals(3, arrayIndexedCollection.size());

		assertFalse(arrayIndexedCollection.remove("D"));

		assertTrue(arrayIndexedCollection.contains("A"));
		assertTrue(arrayIndexedCollection.contains("B"));
		assertTrue(arrayIndexedCollection.contains("C"));
		assertFalse(arrayIndexedCollection.contains("D"));
		assertEquals(3, arrayIndexedCollection.size());
	}

	@Test
	public void collectionThreeElementsRemoveNull() {
		Collection<String> arrayIndexedCollection = prepareOtherCollection();

		assertTrue(arrayIndexedCollection.contains("A"));
		assertTrue(arrayIndexedCollection.contains("B"));
		assertTrue(arrayIndexedCollection.contains("C"));
		assertEquals(3, arrayIndexedCollection.size());

		assertFalse(arrayIndexedCollection.remove(null));

		assertTrue(arrayIndexedCollection.contains("A"));
		assertTrue(arrayIndexedCollection.contains("B"));
		assertTrue(arrayIndexedCollection.contains("C"));
		assertEquals(3, arrayIndexedCollection.size());
	}

	@Test
	public void emptyCollectionToArray() {
		ArrayIndexedCollection<String> arrayIndexedCollection = new ArrayIndexedCollection<>();
		Object[] colArray = arrayIndexedCollection.toArray();

		assertEquals(0, colArray.length);
	}

	@Test
	public void threeElementsCollectionToArray() {
		Object[] colArray = prepareOtherCollection().toArray();

		assertEquals(3, colArray.length);
	}

	@Test
	public void emptyCollectionForEach() {
		ArrayIndexedCollection<String> arrayIndexedCollection = new ArrayIndexedCollection<>();
		assertEquals(0, arrayIndexedCollection.size());
		
		LocalProcessor<String> processor = new LocalProcessor<>();
		arrayIndexedCollection.forEach(processor);
		
		assertEquals(0, processor.counter);
	}

	@Test
	public void threeElementsCollectionForEach() {
		Collection<String> arrayIndexedCollection = prepareOtherCollection();
		assertEquals(3, arrayIndexedCollection.size());

		LocalProcessor<String> processor = new LocalProcessor<>();
		arrayIndexedCollection.forEach(processor);
		
		assertEquals(3, processor.counter);
	}

	private static class LocalProcessor<T> implements Processor<T> {

		int counter;

		public LocalProcessor() {
		}

		public void process(T value) {
			counter++;
		}
	}
	
	
	
	@Test
	public void emptyCollectionClear() {
		ArrayIndexedCollection<String> arrayIndexedCollection = new ArrayIndexedCollection<>();
		assertEquals(0, arrayIndexedCollection.size());

		arrayIndexedCollection.clear();
		
		assertEquals(0, arrayIndexedCollection.size());
	}
	
	@Test
	public void threeElementsCollectionClear() {
		Collection<String> arrayIndexedCollection = prepareOtherCollection();
		assertEquals(3, arrayIndexedCollection.size());

		arrayIndexedCollection.clear();
		
		assertEquals(0, arrayIndexedCollection.size());
	}
	
	
	
	@Test
	public void emptyCollectionGetNegativeIndexElement() {
		ArrayIndexedCollection<String> arrayIndexedCollection = new ArrayIndexedCollection<>();
		assertEquals(0, arrayIndexedCollection.size());
		
		assertThrows(IndexOutOfBoundsException.class, () -> arrayIndexedCollection.get(-1));
	}
	
	@Test
	public void emptyCollectionGetPositiveIndexElement() {
		ArrayIndexedCollection<String> arrayIndexedCollection = new ArrayIndexedCollection<>();
		assertEquals(0, arrayIndexedCollection.size());
		
		assertThrows(IndexOutOfBoundsException.class, () -> arrayIndexedCollection.get(1));
	}
	
	@Test
	public void threeElementsCollectionGetNegativeIndexElement() {
		ArrayIndexedCollection<String> arrayIndexedCollection = prepareOtherCollection();
		assertEquals(3, arrayIndexedCollection.size());
		
		assertThrows(IndexOutOfBoundsException.class, () -> arrayIndexedCollection.get(-1));
	}
	
	@Test
	public void threeElementsCollectionGetPositiveOutOfSizeIndex() {
		ArrayIndexedCollection<String> arrayIndexedCollection = prepareOtherCollection();
		assertEquals(3, arrayIndexedCollection.size());
		
		assertThrows(IndexOutOfBoundsException.class, () -> arrayIndexedCollection.get(3));
	}
	
	
	@Test
	public void insertElementsIntoEmptyCollection() {
		ArrayIndexedCollection<String> arrayIndexedCollection = new ArrayIndexedCollection<>();
		assertEquals(0, arrayIndexedCollection.size());
		
		arrayIndexedCollection.insert("R", 0);
		arrayIndexedCollection.insert("Q", 1);
		
		assertEquals(2, arrayIndexedCollection.size());
	}
	
	@Test
	public void insertElementsIntoEmptyCollectionNegativeIndex() {
		ArrayIndexedCollection<String> arrayIndexedCollection = new ArrayIndexedCollection<>();
		assertEquals(0, arrayIndexedCollection.size());
		
		assertThrows(IndexOutOfBoundsException.class, () -> arrayIndexedCollection.insert("R", -1));
	}
	
	@Test
	public void insertElementsIntoEmptyCollectionPositiveOutOfSizeIndex() {
		ArrayIndexedCollection<String> arrayIndexedCollection = new ArrayIndexedCollection<>();
		assertEquals(0, arrayIndexedCollection.size());
		
		assertThrows(IndexOutOfBoundsException.class, () -> arrayIndexedCollection.insert("R", 1));
	}
	
	@Test
	public void insertElementsIntoThreeElementsCollection() {
		ArrayIndexedCollection<String> arrayIndexedCollection = prepareOtherCollection();
		assertEquals(3, arrayIndexedCollection.size());
		
		arrayIndexedCollection.insert("R", 3);
		arrayIndexedCollection.insert("Q", 4);

		assertEquals(5, arrayIndexedCollection.size());
	}
	
	@Test
	public void insertElementsIntoThreeElementsCollectionNegativeIndex() {
		ArrayIndexedCollection<String> arrayIndexedCollection = prepareOtherCollection();
		assertEquals(3, arrayIndexedCollection.size());
		
		assertThrows(IndexOutOfBoundsException.class, () -> arrayIndexedCollection.insert("R", -1));
	}
	
	@Test
	public void insertElementsIntoThreeElementsCollectionPositiveOutOfSizeIndex() {
		ArrayIndexedCollection<String> arrayIndexedCollection = prepareOtherCollection();
		assertEquals(3, arrayIndexedCollection.size());
		
		assertThrows(IndexOutOfBoundsException.class, () -> arrayIndexedCollection.insert("R", 4));
	}
	
	
	
	@Test
	public void indexOfElementsInEmptyCollection() {
		ArrayIndexedCollection<String> arrayIndexedCollection = new ArrayIndexedCollection<>();
		assertEquals(0, arrayIndexedCollection.size());
		
		assertEquals(-1, arrayIndexedCollection.indexOf(4));
		assertEquals(-1, arrayIndexedCollection.indexOf(null));
	}
	
	@Test
	public void indexOfExistingElementsInThreeElementsCollection() {
		ArrayIndexedCollection<String> arrayIndexedCollection = prepareOtherCollection();
		assertEquals(3, arrayIndexedCollection.size());
		
		assertEquals(0, arrayIndexedCollection.indexOf("A"));
		assertEquals(1, arrayIndexedCollection.indexOf("B"));
		assertEquals(2, arrayIndexedCollection.indexOf("C"));
	}
	
	@Test
	public void indexOfNonexistingElementsInThreeElementsCollection() {
		ArrayIndexedCollection<String> arrayIndexedCollection = prepareOtherCollection();
		assertEquals(3, arrayIndexedCollection.size());
		
		assertEquals(-1, arrayIndexedCollection.indexOf(4));
		assertEquals(-1, arrayIndexedCollection.indexOf(null));
	}
	
	
	
	@Test
	public void removeAtIndexFromEmptyCollection() {
		ArrayIndexedCollection<String> arrayIndexedCollection = new ArrayIndexedCollection<>();
		assertEquals(0, arrayIndexedCollection.size());
		
		assertThrows(IndexOutOfBoundsException.class, () -> arrayIndexedCollection.remove(0));
	}
	
	@Test
	public void removeAtAllowedIndexFromThreeElementCollection() {
		ArrayIndexedCollection<String> arrayIndexedCollection = prepareOtherCollection();
		assertEquals(3, arrayIndexedCollection.size());
		
		arrayIndexedCollection.remove(2);
		assertEquals(2, arrayIndexedCollection.size());
		arrayIndexedCollection.remove(1);
		assertEquals(1, arrayIndexedCollection.size());
		arrayIndexedCollection.remove(0);
		assertEquals(0, arrayIndexedCollection.size());
	}
	
	
	
	private ArrayIndexedCollection<String> prepareOtherCollection() {
		ArrayIndexedCollection<String> arrayIndexedCollection = new ArrayIndexedCollection<>();
		assertEquals(16, ArrayIndexedCollection.getCapacity());
		assertEquals(0, arrayIndexedCollection.size());
		assertNotNull(arrayIndexedCollection);

		arrayIndexedCollection.add(new String("A"));
		arrayIndexedCollection.add(new String("B"));
		arrayIndexedCollection.add(new String("C"));

		assertEquals(3, arrayIndexedCollection.size());

		return arrayIndexedCollection;
	}

}
