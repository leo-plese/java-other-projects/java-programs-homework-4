package hr.fer.zemris.java.custom.collections;

/**
 * Interface representing getter for collection elements. 
 * 
 * @author Leo
 *
 */
public interface ElementsGetter<T> {
	
	/**
	 * Method determining if there is next element to iterate over.
	 * 
	 * @return true if there is next element to iterate over
	 */
	boolean hasNextElement();
	
	/**
	 * Method returning the next element to iterate over.
	 * 
	 * @return next element to iterate over
	 */
	T getNextElement();
	
	/**
	 * Method processing all the elements <code>ElementsGetter</code> is iterating over executing actions given by <code>Processor</code>.
	 * It does so as long as there is elements to iterate over.
	 * 
	 * @param p processor giving the action to be performed on the elements the elements getter iterates over
	 */
	default void processRemaining(Processor<T> p) {
		while (hasNextElement()) {
			p.process(getNextElement());
		}
	}

}
