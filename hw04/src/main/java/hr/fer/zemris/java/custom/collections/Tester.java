package hr.fer.zemris.java.custom.collections;

/**
 * 
 * Interface representing model for testing an object against a condition.
 *  
 * @author Leo
 *
 */
public interface Tester<T> {
	
	/**
	 * Method for testing if the given object fulfills condition given by the <code>Tester</code>.
	 * 
	 * @param obj object to be tested upon the condition
	 * @return true if object fulfills given condition
	 */
	boolean test(T obj);
}
