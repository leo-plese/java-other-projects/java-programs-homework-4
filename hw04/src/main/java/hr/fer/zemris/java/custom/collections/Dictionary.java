package hr.fer.zemris.java.custom.collections;

import java.util.Objects;

/**
 * Class <code>Dictionary</code> represents collection model of a key-value pairs.
 * 
 * @author Leo
 *
 * @param <K> parameter representing dictionary key from a key-value pair
 * @param <V> parameter representing dictionary value from a key-value pair
 */
public class Dictionary<K, V> {
	
	/**
	 * internal collection used by a <code>Dictionary</code> object for storing key-value pairs
	 */
	private ArrayIndexedCollection<KeyValueTuple<K,V>> arrayIndexedCollection = new ArrayIndexedCollection<>();
	
	/**
	 * Class representing model of a single key-value pair.
	 * 
	 * @author Leo
	 *
	 * @param <K> parameter representing dictionary key from a key-value pair
	 * @param <V> parameter representing dictionary key from a key-value pair
	 */
	private static class KeyValueTuple<K, V> {
		/**
		 * key in a key-value pair
		 */
		private K key;
		/**
		 * value in a key-value pair
		 */
		private V value;
		
		/**
		 * constructor given key and value to construct the key-value pair
		 * 
		 * Important: key must not be null
		 * 
		 * @param key key to construct the key-value pair
		 * @param value value to construct the key-value pair
		 * @throws NullPointerException if given key is null
		 */
		public KeyValueTuple(K key, V value) {
			super();
			this.key = Objects.requireNonNull(key);
			this.value = value;
		}
		
		/**
		 * Method getting value from the key-value pair.
		 * 
		 * @return value from the key-value pair
		 */
		public V getValue() {
			return value;
		}

		/**
		 * Method setting value of a key-value pair
		 * 
		 * @param value new value to update the key-value pair with
		 */
		public void setValue(V value) {
			this.value = value;
		}

		/**
		 * Method getting key from the key-value pair.
		 * 
		 * @return key from the key-value pair
		 */
		public K getKey() {
			return key;
		}
	}
	
	/**
	 * Method determining if a <code>Dictionary</code> is empty i.e. has no key-value pairs stored
	 * 
	 * @return true if dictionary is empty
	 */
	public boolean isEmpty() {
		return arrayIndexedCollection.isEmpty();
	}
	
	/**
	 * Method returning size of a <code>Dictionary</code> i.e. number of key-value pairs stored
	 * @return dictionary size
	 */
	public int size() {
		return arrayIndexedCollection.size();
	}
	
	/**
	 * Method clearing a dictionary i.e. removing all the key-value pairs from it thus making it empty.
	 */
	public void clear() {
		arrayIndexedCollection.clear();
	}
	
	/**
	 * Method putting a new key-value pair into the dictionary. If already present, the value of the key-value pair is appropriately updated with the new given value.
	 * 
	 * @param key key of the key-value pair
	 * @param value value to update the existing key value pair if it already exists, or to make a new key-value pair with given key if the key is a new one
	 * @throws NullPointerException if key is null (unallowed value for key)
	 */
	public void put(K key, V value) {
		Objects.requireNonNull(key);
		
		int keyInd = containsKey(key);
		if (keyInd != -1) {
			KeyValueTuple<K,V> kv = arrayIndexedCollection.get(keyInd);
			kv.setValue(value);
			return;
		}
		
		arrayIndexedCollection.add(new KeyValueTuple<K,V>(key, value));
	}
	
	/**
	 * Method getting value from the key-value pair specified by its key.
	 * 
	 * @param key key determining the key-value pair to get the value from 
	 * @return value from the key-value pair determined by the key
	 * @throws NullPointerException if key is null (unallowed value for key)
	 */
	public V get(Object key) {
		Objects.requireNonNull(key);
		
		int keyInd = containsKey(key);
		
		if (keyInd == -1) {
			return null;
		}
		
		return arrayIndexedCollection.get(keyInd).getValue();
	}
	
	/**
	 * Method determining if the given key is contained in the dictionary in a key-value pair.
	 * 
	 * @param key key to be tested if it is contained in the dictionary
	 * @return true if the key is contained in the dictionary
	 */
	private int containsKey(Object key) {
		for (int i = 0, n = arrayIndexedCollection.size(); i < n; i++) {
			KeyValueTuple<K,V> kv = arrayIndexedCollection.get(i);
			if (kv.getKey().equals(key)) {
				return i;
			}
		}
		return -1;
	}
	
	

}
