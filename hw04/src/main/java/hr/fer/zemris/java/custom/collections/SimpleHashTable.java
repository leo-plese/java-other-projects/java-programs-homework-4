package hr.fer.zemris.java.custom.collections;

import java.util.Arrays;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;

/**
 * Class representing model of a hash table which implements <code>Iterable</code> for iterating over its table entries. 
 * 
 * @author Leo
 *
 * @param <K> key type of a key-value pairs
 * @param <V> value type of a key-value pairs
 */
public class SimpleHashTable<K, V> implements Iterable<SimpleHashTable.TableEntry<K, V>> {

	/**
	 * internal array of hash table slots containing table entries
	 */
	private TableEntry<K, V>[] entryArray;
	/**
	 * size of a hashtable - number of entries stored in it
	 */
	private int size = 0;
	/**
	 * default number of slots in a hashtable
	 */
	private static final int DEFAULT_SLOTS = 16;
	/**
	 * hashtable load factor default - important to determine when hashtable needs to reallocate to redistribute its entries over slots for table entries to become more uniformly distributed over the hashtable 
	 */
	private static final double UPPER_LIMIT_LOAD_FACTOR = 0.75;
	/**
	 * variable keeping track of modifications done on hashtable e.g. adding a new key value pair, adding a key value pair to beginning of a slot, removing a key-value pair, clearing the hashtable
	 */
	private long modificationCount = 0;

	/**
	 * default constructor - initializes hashtable entry array to default initial capacity <code>DEFAULT_SLOTS</code>
	 */
	@SuppressWarnings("unchecked")
	public SimpleHashTable() {
		super();
		entryArray = (TableEntry<K, V>[]) new TableEntry[DEFAULT_SLOTS];
	}

	/**
	 * constructor initializing hashtable entry array to given initial capacity rounded to the next greater power of 2 (or to the very given capacity if it is already a power of 2)
	 * 
	 * @param capacity given initial capacity of a hashtable - must be greater than or equal to 1
	 * @throws IllegalArgumentException if given initial capacity of a hashtable is less than 1
	 */
	@SuppressWarnings("unchecked")
	public SimpleHashTable(int capacity) {
		super();
		if (capacity < 1) {
			throw new IllegalArgumentException("Initial table capacity must be greater than or equal to 1!");
		}
		int initialCapPower2 = (int) Math.pow(2, Math.ceil(Math.log(capacity) / Math.log(2)));
		entryArray = (TableEntry<K, V>[]) new TableEntry[initialCapPower2];

	}

	/**
	 * Method calling helper method <code>put(K, V, TableEntry<K, V>[], boolean)<code> to put a key-value pair in the hashtable based onto the position determined by the hashcode of the given key and hashtable capacity.
	 * The key-value pair can be updated if there is already the given key contained in the table, or added if there is not one yet.
	 * 
	 * Note: the last input "true" means the track of modifications must be kept
	 * 
	 * @param key key determining key-value pair to be updated/added 
	 * @param value value of the key-value pair
	 * @throws NullPointerException if given key is null
	 */
	public void put(K key, V value) {
		put(key, value, entryArray, true);
	}

	/**
	 * Helper method putting a key-value pair in the hashtable based onto the position determined by the hashcode of the given key and hashtable capacity.
	 * The key-value pair can be updated if there is already the given key contained in the table, or added if there is not one yet.
	 * 
	 * @param key key determining key-value pair to be updated/added
	 * @param value value of the key-value pair
	 * @param entryArr array to put the key-value pair into
	 * @param isModifying flag variable determining if <code>modificationCount</code> is going to increase (true in case the <code>put(K, V)</code> method is called from outside this class)
	 * @throws NullPointerException if given key is null
	 */
	private void put(K key, V value, TableEntry<K, V>[] entryArr, boolean isModifying) {
		Objects.requireNonNull(key, "Key must not be null!");

		TableEntry<K, V> entry = getEntryFromSlot(key, entryArr);
		int sn = getSlotNumber(key, entryArr);

		TableEntry<K, V> ent = entryArr[sn];
		int cnt = 0;
		while (ent != null) {
			cnt++;
			ent = ent.next;
		}
		if (cnt == 0) {

			entry = new TableEntry<>(key, value, null);
			entryArr[sn] = entry;
		
			
			if (isModifying) {
				size++;
				modificationCount++;
				checkHashTableLoadFactor();
			}

			return;
		}

		if (entry.key.equals(key)) {
			entry.value = value;
			return;
		}

		while (entry.next != null) {
			entry = entry.next;
			if (key.equals(entry.key)) {
				entry.value = value;
				return;
			}

		}

		entry.next = new TableEntry<K, V>(key, value, null);

	
		
		if (isModifying) {
			size++;
			modificationCount++;
			checkHashTableLoadFactor();
		}

	}

	/**
	 * Method checking if current load factor (proportion of hashtable size and its capacity) is greater than allowed.
	 * If it is the case, it allocates a new array of table entries and puts the values from the <code>entryArray</code> with appropriate new hashes in the new entry array of double the capacity of the first one.
	 * In the end, <code>entryArray</code> must be cleared and, finally, assigned the value of the new array so as to get the wanted resulting hashtable.
	 */
	@SuppressWarnings("unchecked")
	private void checkHashTableLoadFactor() {
		if (size >= UPPER_LIMIT_LOAD_FACTOR * entryArray.length) {
			TableEntry<K, V>[] newEntryArray = (TableEntry<K, V>[]) new TableEntry[2 * entryArray.length];
			for (TableEntry<K, V> entry : entryArray) {
				while (entry != null) {
					put(entry.key, entry.value, newEntryArray, false);
					entry = entry.next;
				}
			}

			clear(false);

			entryArray = Arrays.copyOf(newEntryArray, newEntryArray.length);
		}
	}

	/**
	 * Method calling helper method <code>clear(boolean)</code> to clear the hashtable i.e. remove all the table entries from it.
	 *
	 * Note: the input "true" means the track of modifications must be kept
	 */
	public void clear() {
		clear(true);
	}

	/**
	 * Helper method clearing the hashtable i.e. removing all the table entries from it.
	 * 
	 * @param isModifying flag variable determining if <code>modificationCount</code> is going to increase (true in case the <code>clear()</code> method is called from outside this class)
	 */
	private void clear(boolean isModifying) {
		
		for (int i = 0, n = entryArray.length; i < n; i++) {
			TableEntry<K, V> entry = entryArray[i];

			while (entry != null) {
				remove(entry.key, isModifying);
				entry = entryArray[i];
			}
		}
	}

	/**
	 * Method getting value from the key-value pair stored in a hashtable table entry determined by the input key.
	 * 
	 * Note: Gets nothing (returns <code>null</code>) if key is not contained in a hashtable entry or if key is null
	 * 
	 * @param key key from the key-value pair to get the value from
	 * @return value from the key-value pair determined by the given key
	 */
	public V get(Object key) {
		if (!containsKey(key)) {
			return null;
		}

		TableEntry<K, V> entry = getEntryFromSlot(key, entryArray);

		while (entry != null) {
			if (key.equals(entry.key)) {
				break;
			}
			entry = entry.next;
		}

		return entry.value;
	}

	/**
	 * Method getting reference to the first table entry in a hashtable slot determined by the key.
	 * Calls <code>getSlotNumber(Object, TableEntry<K, V>[])</code> to get the slot number determined by the key.
	 * 
	 * @param key key based on which slot number is calculated and the slot is returned
	 * @param entryArr array to get the entry from
	 * @return hashtable entry with given key
	 */
	private TableEntry<K, V> getEntryFromSlot(Object key, TableEntry<K, V>[] entryArr) {
		int slotNr = getSlotNumber(key, entryArr);
		return entryArr[slotNr];
	}

	/**
	 * Method returning hashtable slot number based on key's hashcode and table capacity using modulo operator to get a valid index of a slot in table entries array. 
	 * 
	 * @param key key whose hashcode is to be calculated
	 * @param entryArr table entries array whose slot number is calculated
	 * @return number of the hashtable slot determined by the key
	 */
	private int getSlotNumber(Object key, TableEntry<K, V>[] entryArr) {
		return Math.abs(key.hashCode()) % entryArr.length;
	}

	/**
	 * size of a hashtable - number of entries stored in it
	 * @return
	 */
	public int size() {
		return size;
	}

	/**
	 * Method saying if the given key is contained in a hashtable entry.
	 * 
	 * Note: if key is null, it is not contained (key value null is unallowed)
	 * 
	 * @param key key whose presence is being checked upon
	 * @return true if a table entry contains the input key
	 */
	public boolean containsKey(Object key) {
		if (key == null) {
			return false;
		}

		TableEntry<K, V> entry = getEntryFromSlot(key, entryArray);

		while (entry != null) {
			if (key.equals(entry.key)) {
				return true;
			}

			entry = entry.next;
		}

		return false;
	}

	/**
	 * Method saying if the given value is contained in a hashtable entry.
	 * 
	 * @param value value whose presence is being checked upon
	 * @return true if a table entry contains the input value
	 */
	public boolean containsValue(Object value) {
		for (TableEntry<K, V> entry : entryArray) {
			while (entry != null) {
				V val = entry.value;
				if (val == null) {
					if (value == null) {
						return true;
					}
					entry = entry.next;
					continue;
				}
				if (val.equals(value)) {
					return true;
				}
				entry = entry.next;
			}
		}

		return false;
	}
	
	/**
	 * Method calling helper method <code>remove(Object, boolean)</code> to remove the key-value pair table entry determined by given key from the hashtable.
	 * 
	 * Note: the input "true" means the track of modifications must be kept
	 * 
	 * @param key key determining table entry which is to be deleted
	 */
	public void remove(Object key) {
		remove(key, true);
	}
	
	/**
	 * Helper method removing the key-value pair table entry determined by given key from the hashtable.
	 * 
	 * Note: It does nothing if the key is not contained in some hashtable table entry.
	 * 
	 * @param key key determining table entry which is to be deleted
	 * @param isModifying flag variable determining if <code>modificationCount</code> is going to increase (true in case the <code>clear()</code> method is called from outside this class)
	 */
	private void remove(Object key, boolean isModifying) {
		if (!containsKey(key)) {
			return;
		}

		TableEntry<K, V> entry = getEntryFromSlot(key, entryArray);

		while (entry != null) {
			if (key.equals(entry.key)) {
				TableEntry<K, V> old = entry;
				entry = entry.next;
				entryArray[getSlotNumber(key, entryArray)] = entry;
				old.next = null;
				old = null;
				break;
			}
			if (entry.next != null && key.equals(entry.next.key)) {
				TableEntry<K, V> old = entry.next;

				entry.next = entry.next.next;
				old.next = null;
				old = null;
				break;
			}
			entry = entry.next;
		}

		if (isModifying) {
			size--;
			modificationCount++;
		}
	}
	

	/**
	 * Method determining if hashtable is empty i.e. has no entries stored in it.
	 * 
	 * @return true if hashtable is empty 
	 */
	public boolean isEmpty() {
		return size == 0;
	}

	/**
	 * String representation of SimpleHashTable in format [key1=value1, key2=value2].
	 */
	public String toString() {
		if (size == 0) {
			return "[]";
		}

		StringBuilder sb = new StringBuilder("[");

		for (int i = 0, n = entryArray.length; i < n; i++) {
			TableEntry<K, V> entry = entryArray[i];
			while (entry != null) {
				sb.append(entry.key + "=" + entry.value);
				if (size == 1 || i == n - 1 && entry.next == null) {
					break;
				}
				sb.append(", ");
				entry = entry.next;
			}
			//uncomment next line if you wish to see slots delimited by |i| where i represents slot number
			//sb.append("|"+i+"|");
		}
		sb.append("]");
		return sb.toString();
	}

	/**
	 * 
	 * Class representing single table entry of hashtable.
	 * 
	 * @author Leo
	 *
	 * @param <K> key parameter type of a key-value pair
	 * @param <V> value parameter type of a key-value pair
	 */
	public static class TableEntry<K, V> {
		/**
		 * key of a key-value pair
		 */
		private K key;
		/**
		 * value of a key-value pair
		 */
		private V value;
		/**
		 * reference to next table entry in the slot
		 */
		private TableEntry<K, V> next;

		/**
		 * constructor given key and value to construct the key-value pair and with reference to next table entry in the slot
		 * 
		 * @param key key of a key-value pair
		 * @param value value of a key-value pair
		 * @param next reference to next table entry in the slot
		 */
		public TableEntry(K key, V value, TableEntry<K, V> next) {
			super();
			this.key = key;
			this.value = value;
			this.next = next;
		}

		/**
		 * Method getting value from the table entry.
		 * 
		 * @return value from the table entry
		 */
		public V getValue() {
			return value;
		}

		/**
		 * Method setting value of the table entry.
		 * @param value value of the table entry
		 */
		public void setValue(V value) {
			this.value = value;
		}

		/**
		 * Method getting key from the table entry.
		 * 
		 * @return key from the table entry
		 */
		public K getKey() {
			return key;
		}

	}

	/**
	 * Method getting an <code>Iterator</code> iterator object for iterating over hashtable table entries.
	 */
	@Override
	public Iterator<TableEntry<K, V>> iterator() {
		return new IteratorImpl();
	}

	/**
	 * Class implementing <code>Iterator</code> over <code>SimpleHashTable.TableEntry<K, V></code> hashtable table entries.
	 * 
	 * @author Leo
	 *
	 */
	private class IteratorImpl implements Iterator<SimpleHashTable.TableEntry<K, V>> {

		/**
		 * current slot index entries are being iterated over
		 */
		private int slotIndex;
		/**
		 * current table entry in current table slot the iterator has iterated over
		 */
		private TableEntry<K, V> currentEntry;
		/**
		 * last entry returned by <code>next()</code> method
		 */
		private TableEntry<K, V> lastReturned;
		/**
		 * flag variable determining if <code>remove()</code> method can remove the element last returned by <code>next()</code> 
		 */
		private boolean hasEntryToRemove = false;
		/**
		 * variable storing initial count of modifications done on the hashtable - increased only in <code>remove()</code> where it is necessary
		 */
		private long savedModificationCount;

		/**
		 * constructor storing initial value of table modification count
		 */
		public IteratorImpl() {
			savedModificationCount = modificationCount;
		}

		/**
		 * Method determining if there is one more element to iterate over.
		 * 
		 * @return true if there is one more element to iterate over
		 * @throws ConcurrentModificationException if hashtable has been changed since the iterator constructor or the last removing of an element done by the iterator
		 */
		@Override
		public boolean hasNext() {
			if (modificationCount != savedModificationCount) {
				throw new ConcurrentModificationException();
			}
			
			if (currentEntry == null) {
				if (slotIndex + 1 == entryArray.length) {
					return false;
				}
				
				do {
					slotIndex++;
					if (slotIndex >= entryArray.length) {
						return false;
					}
					currentEntry = entryArray[slotIndex];
				} while (currentEntry == null);
			}
			return true;
		}

		/**
		 * Method getting next element (table entry from the hashtable <code>SimpleHashTable</code>) being iterated.
		 * 
		 * @return next table entry being iterated over
		 * @throws ConcurrentModificationException if hashtable has been changed since the iterator constructor or the last removing of an element done by the iterator
		 * @throws NoSuchElementException if there is no more elements to iterate over
		 */
		@Override
		public TableEntry<K, V> next() {
			if (modificationCount != savedModificationCount) {
				throw new ConcurrentModificationException();
			}
			
			if (!hasNext()) {
				throw new NoSuchElementException();
			}

			lastReturned = currentEntry;
			currentEntry = currentEntry.next;
			hasEntryToRemove = true;
			return lastReturned;
		}

		/**
		 * Method removing last element returned by <code>next()</code> method.
		 * 
		 * Cannot be called more than once in a row.
		 * 
		 * @throws ConcurrentModificationException if hashtable has been changed since the iterator constructor or the last removing of an element done by the iterator
		 * @throws IllegalStateException if there is an attempt to call the method more than once in a row
		 */
		@Override
		public void remove() {
			if (modificationCount != savedModificationCount) {
				throw new ConcurrentModificationException();
			}
			
			if (!hasEntryToRemove) {
				throw new IllegalStateException("Cannot remove "+lastReturned.key+" more than once in a row!");
			}
			SimpleHashTable.this.remove(lastReturned.key);
			savedModificationCount++;
			hasEntryToRemove = !hasEntryToRemove;
		}

	}
}
