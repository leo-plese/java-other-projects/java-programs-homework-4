package hr.fer.zemris.java.custom.collections.demo;

//following imports are necessary for executing given tests
import java.util.Iterator;
import hr.fer.zemris.java.custom.collections.SimpleHashTable;

/**
 * Class containing second part of official tests for <code>SimpleHashTable</code> class which use <code>Iterator</code>.
 * 
 * Please note:
 * All of the tests are commented out but in case of testing, they can be uncommented.
 * 
 * @author Leo
 *
 */
public class SimpleHashTableIteratorTester {

	/**
	 * Main method containing tests for <code>SimpleHashTable</code> class which use <code>Iterator</code>.
	 * 
	 * Please, feel free to uncomment any one by one at a time to see them function.
	 * 
	 * @param args command line arguments not used in this program
	 */
	public static void main(String[] args) {

		// following tests from document

		///////////// TEST 1
//		// create collection:
//		SimpleHashTable<String, Integer> examMarks = new SimpleHashTable<>(2);
//		// fill data:
//		examMarks.put("Ivana", 2);
//		examMarks.put("Ante", 2);
//		examMarks.put("Jasna", 2);
//		examMarks.put("Kristina", 5);
//		examMarks.put("Ivana", 5); // overwrites old grade for Ivana
//		for (SimpleHashTable.TableEntry<String, Integer> pair : examMarks) {
//			System.out.printf("%s => %d%n", pair.getKey(), pair.getValue());
//		}
//		System.out.println();

		
		/////////// TEST 2
//		// create collection:
//		SimpleHashTable<String, Integer> examMarks = new SimpleHashTable<>(2);
//		// fill data:
//		examMarks.put("Ivana", 2);
//		examMarks.put("Ante", 2);
//		examMarks.put("Jasna", 2);
//		examMarks.put("Kristina", 5);
//		examMarks.put("Ivana", 5); // overwrites old grade for Ivana
//		for (SimpleHashTable.TableEntry<String, Integer> pair1 : examMarks) {
//			for (SimpleHashTable.TableEntry<String, Integer> pair2 : examMarks) {
//				System.out.printf("(%s => %d) - (%s => %d)%n", pair1.getKey(), pair1.getValue(), pair2.getKey(),
//						pair2.getValue());
//			}
//		}
//		System.out.println();

		/////////// TEST 3
//		// create collection:
//		SimpleHashTable<String, Integer> examMarks = new SimpleHashTable<>(2);
//		// fill data:
//		examMarks.put("Ivana", 2);
//		examMarks.put("Ante", 2);
//		examMarks.put("Jasna", 2);
//		examMarks.put("Kristina", 5);
//		examMarks.put("Ivana", 5); // overwrites old grade for Ivana
//		System.out.println("examMarks contains key Ivana before removing: " + examMarks.containsKey("Ivana"));
//		Iterator<SimpleHashTable.TableEntry<String, Integer>> iter = examMarks.iterator();
//		while (iter.hasNext()) {
//			SimpleHashTable.TableEntry<String, Integer> pair = iter.next();
//			if (pair.getKey().equals("Ivana")) {
//				iter.remove();
//				// uncomment next line to see IllegalStateException
//				//iter.remove();
//			}
//		}
//		System.out.println("examMarks contains key Ivana after removing: " + examMarks.containsKey("Ivana"));
//		System.out.println();
		
		
		/////////// TEST 4 (note: should throw a ConcurrentModificationException)
//		SimpleHashTable<String, Integer> examMarks = new SimpleHashTable<>(2);		
//		examMarks.put("Ivana", 2);
//		examMarks.put("Ante", 2);
//		examMarks.put("Jasna", 2);
//		examMarks.put("Kristina", 5);
//		examMarks.put("Ivana", 5); // overwrites old grade for Ivana
//		Iterator<SimpleHashTable.TableEntry<String, Integer>> iter = examMarks.iterator();
//		while (iter.hasNext()) {
//			SimpleHashTable.TableEntry<String, Integer> pair = iter.next();
//			if (pair.getKey().equals("Ivana")) {
//				//please note, next line should throw a ConcurrentModificationException
//				examMarks.remove("Ivana"); //-->ConcurrentModificationException
//			}
//		}

		/////////// TEST 5
//		SimpleHashTable<String, Integer> examMarks = new SimpleHashTable<>(2);
//
//		examMarks.put("Ivana", 2);
//		examMarks.put("Ante", 2);
//		examMarks.put("Jasna", 2);
//		examMarks.put("Kristina", 5);
//		examMarks.put("Ivana", 5); // overwrites old grade for Ivana
//
//		Iterator<SimpleHashTable.TableEntry<String, Integer>> iter = examMarks.iterator();
//		System.out.printf("Size: %d%n", examMarks.size());
//
//		while (iter.hasNext()) {
//			SimpleHashTable.TableEntry<String, Integer> pair = iter.next();
//			System.out.printf("%s => %d%n", pair.getKey(), pair.getValue());
//			iter.remove();
//		}
//
//		System.out.printf("Size: %d%n", examMarks.size());

	}

}
