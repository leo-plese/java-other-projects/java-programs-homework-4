package hr.fer.zemris.math;

import java.util.Objects;

/**
 * Class representing model of a 2D vector represented by its x and y components.
 * 
 * @author Leo
 *
 */
public class Vector2D {
	
	/**
	 * x component of a 2D vector
	 */
	private double x;
	/**
	 * y component of a 2D vector
	 */
	private double y;
	
	/**
	 * constructor constructing 2D vector based on its x and y components
	 * @param x x component of a 2D vector
	 * @param y y component of a 2D vector
	 * 
	 * @throws NullPointerException if either x or y component is null
	 */
	public Vector2D(double x, double y) {
		super();
		this.x = Objects.requireNonNull(x);
		this.y = Objects.requireNonNull(y);
	}
	
	/**
	 * constructor constructing a new vector based on another one
	 * 
	 * @param vector2D another vector to constructor a new one from
	 */
	private Vector2D(Vector2D vector2D) {
		this(vector2D.x, vector2D.y);
	}
	
	/**
	 * Method getting x component of a 2D vector.
	 * 
	 * @return x component of a 2D vector
	 */
	public double getX() {
		return x;
	}
	
	/**
	 * Method getting y component of a 2D vector.
	 * 
	 * @return y component of a 2D vector
	 */
	public double getY() {
		return y;
	}
	
	/**
	 * Method getting angle of a vector given by arctan of its y and x components proportion.
	 * 
	 * Note: angle from range [0, 2*PI>
	 * 
	 * @return angle of a vector
	 */
	public double getAngle() {
		if (x == 0 && y == 0) {
			return 0;
		} else if (x == 0) {
			return y > 0 ? Math.PI/2 : 3*Math.PI/2;
		} else if (y == 0) {
			return x > 0 ? 0 : Math.PI;
		}
		
		double angle = Math.atan(y / x);
		
		if (x < 0) {
			angle += Math.PI;
			return angle;
		}
	
		
		if (angle < 0) {
			angle += 2*Math.PI;
			
		}
		return angle;
	
	}
	
	/**
	 * Method getting magnitude of a vector given by square root of sum of its x and y components squared.
	 * @return magnitude of a vector
	 */
	public double getMagnitude() {
		if (x == 0 && y == 0) {
			return 0;
		} else if (x == 0) {
			return Math.abs(y);
		} else if (y == 0) {
			return Math.abs(x);
		}
		return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
	}
	
	/**
	 * Method directly modifying this vector by translating it for given offset vector.
	 * 
	 * @param offset vector for which this vector is being translated for
	 * @throws NullPointerException if input offset vector is null
	 */
	public void translate(Vector2D offset) {
		Objects.requireNonNull(offset);
		x += offset.x;
		y += offset.y;
	}
	
	/**
	 * Method returning a new vector by translating this vector for given offset vector.
	 * 
	 * @param offset vector for vector is being translated for
	 * @return new vector as a result of this translation
	 * @throws NullPointerException if input offset vector is null
	 */
	public Vector2D translated(final Vector2D offset) {
		Objects.requireNonNull(offset);
		return new Vector2D(x + offset.x, y + offset.y);
	}
	
	/**
	 * Method directly modifying this vector by rotating it for given angle.
	 * 
	 * @param angle angle for which this vector is being rotated for
	 * @throws NullPointerException if input angle is null
	 */
	public void rotate(double angle) {
		Objects.requireNonNull(angle);
		double magnitude = getMagnitude();
		double newAngle = getAngle() + angle;
		this.x = magnitude * Math.cos(newAngle);
		this.y = magnitude * Math.sin(newAngle);
	}
	
	/**
	 * Method returning a new vector by rotating this vector for given angle.
	 * 
	 * @param angle angle for which vector is being rotated for
	 * @return new vector as a result of this rotation
	 * @throws NullPointerException if input angle is null
	 */
	public Vector2D rotated(final double angle) {
		Objects.requireNonNull(angle);
		double magnitude = getMagnitude();
		double newAngle = getAngle() + angle;
		return new Vector2D(magnitude * Math.cos(newAngle), magnitude * Math.sin(newAngle));
	}
	
	/**
	 * Method directly modifying this vector by scaling it for given scaling factor.
	 * 
	 * @param scaler scaler factor by which the vector is being scaled by
	 * @throws NullPointerException if input scaler is null
	 */
	public void scale(double scaler) {
		Objects.requireNonNull(scaler);
		this.x *= scaler;
		this.y *= scaler;
	}
	
	/**
	 * Method directly returning a new vector by scaling vector for given factor.
	 * 
	 * @param scaler scaler factor by which the vector is being scaled by
	 * @return new vector as a result of this scaling
	 * @throws NullPointerException if input scaler is null
	 */
	public Vector2D scaled(final double scaler) {
		Objects.requireNonNull(scaler);
		return new Vector2D(x * scaler, y * scaler);
	}
	
	/**
	 * Method returning a new vector object constructed based on this vector.
	 * 
	 * @return new vector object constructed based on this vector
	 */
	public Vector2D copy() {
		return new Vector2D(this);
	}

	/**
	 * Method returning hashcode of a 2D vector.
	 */
	@Override
	public int hashCode() {
		return Objects.hash(x, y);
	}

	/**
	 * Method determining if two 2D vectors are equal.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Vector2D))
			return false;
		Vector2D other = (Vector2D) obj;
		return Double.doubleToLongBits(x) == Double.doubleToLongBits(other.x)
				&& Double.doubleToLongBits(y) == Double.doubleToLongBits(other.y);
	}

	/**
	 * Method returning string representation of a 2D vector.
	 */
	@Override
	public String toString() {
		return "Vector2D [x=" + x + ", y=" + y + "]";
	}

}
